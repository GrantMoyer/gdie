use gdie::interpret_str;
use num_bigint::BigUint;
use num_traits::cast::ToPrimitive;
use std::env;

fn biguint_div(a: &BigUint, b: &BigUint) -> f64 {
	let scale = BigUint::from(10usize).pow(16u32);
	(a * &scale / b)
		.to_f64()
		.unwrap_or_else(|| panic!("Count large for f64: {}", a * &scale))
		/ scale
			.to_f64()
			.unwrap_or_else(|| panic!("Count large for f64: {}", &scale))
}

fn main() {
	let arg = env::args().skip(1).collect::<Vec<_>>().join(" ");
	let res = interpret_str(&arg);
	let total = res.values().sum::<BigUint>();
	for (face, count) in res.iter() {
		println!("{}: {:.3}", face, biguint_div(count, &total))
	}
}

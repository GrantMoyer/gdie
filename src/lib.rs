#[macro_use]
extern crate pest_derive;
extern crate console_error_panic_hook;
use pest::Parser;
use wasm_bindgen::prelude::*;

pub mod parser;

#[wasm_bindgen]
pub fn interpret_js(input: &str) -> JsValue {
	console_error_panic_hook::set_once();
	let map = interpret_str(input);
	let str_map = map
		.into_iter()
		.map(|(face, count)| (face.to_string(), count.to_string()))
		.collect::<std::collections::HashMap<_, _>>();
	JsValue::from_serde(&str_map).expect("Failed to convert random variable to javascript value")
}

pub fn interpret_str(input: &str) -> interp::Map {
	let expr = parser::ExpressionParser::parse(parser::Rule::program, input)
		.unwrap_or_else(|err| panic!("Failed to parse input: {}", err))
		.next()
		.expect("Missing expression body. This is a bug");
	interp::interpret(expr)
}

pub mod interp {
	use crate::parser::Rule;
	use num_bigint::{BigInt, BigUint, RandBigInt};
	use num_traits::{
		cast::ToPrimitive,
		identities::{One, Zero},
	};
	use pest::iterators::Pair;
	use std::collections::HashMap;

	pub type Map = std::collections::BTreeMap<BigInt, BigUint>;

	pub fn interpret(expr: Pair<'_, Rule>) -> Map {
		match expr.as_rule() {
			Rule::application => {
				let mut pairs = expr.into_inner();
				let app = pairs.next().expect("Missing applicand. This is a bug");
				let arg = pairs.next().expect("Missing argument. This is a bug");
				apply(app, arg)
			}
			Rule::operation => {
				let operators = {
					let table: &[(&str, fn(&Map, &Map) -> Map)] = &[
						("+", |l, r| bin_op(l, r, std::ops::Add::add)),
						("-", |l, r| bin_op(l, r, std::ops::Sub::sub)),
						("*", |l, r| bin_op(l, r, std::ops::Mul::mul)),
						("/", |l, r| bin_op(l, r, std::ops::Div::div)),
						("&", |l, r| bin_op(l, r, std::cmp::min)),
						("|", |l, r| bin_op(l, r, std::cmp::max)),
					];
					table.iter().cloned().collect::<HashMap<_, _>>()
				};
				let mut pairs = expr.into_inner();
				let lhs = interpret(
					pairs
						.next()
						.expect("Missing left hand operand. This is a bug"),
				);
				let op = pairs.next().expect("Missing operator. This is a bug");
				assert!(
					op.as_rule() == Rule::operator,
					"Unexpected rule in operator position: {:?}. This is a bug.",
					op.as_rule()
				);
				let rhs = interpret(
					pairs
						.next()
						.expect("Missing left hand operand. This is a bug"),
				);
				let op_f = operators
					.get(op.as_str())
					.unwrap_or_else(|| panic!("Unknown operator: {}", op.as_str()));
				op_f(&lhs, &rhs)
			}
			Rule::integer => {
				let val = expr.as_str().parse().unwrap_or_else(|_| {
					panic!("Failed to parse integer: {}. This is a bug.", expr.as_str())
				});
				con(val)
			}
			Rule::ident => {
				panic!("Identifier without argument: {}", expr.as_str())
			}
			x => panic!("Unexpected rule: {:?}. This is a bug.", x),
		}
	}

	fn apply(app: Pair<'_, Rule>, arg: Pair<'_, Rule>) -> Map {
		match app.as_rule() {
			Rule::ident => {
				let builtins = {
					let table: &[(&str, fn(&Map) -> Map)] = &[
						("d", die),
						("roll", roll),
						("advantage", |a| bin_op(a, a, std::cmp::max)),
						("disadvantage", |a| bin_op(a, a, std::cmp::min)),
					];
					table.iter().cloned().collect::<HashMap<_, _>>()
				};
				let id = app.as_str().to_lowercase();
				if ![Rule::application, Rule::integer, Rule::operation].contains(&arg.as_rule()) {
					unimplemented!("application argument type: {:?}", arg.as_rule());
				}
				let arg_var = interpret(arg);
				let app_f = builtins
					.get(id.as_str())
					.unwrap_or_else(|| panic!("Unknown identifier: {}", id));
				app_f(&arg_var)
			}
			Rule::integer => {
				let count = app.as_str().parse::<BigUint>().unwrap_or_else(|_| {
					panic!("Failed to parse integer: {}. This is a bug.", app.as_str())
				});
				let var = interpret(arg);
				product(&count, &var)
			}
			Rule::application => {
				let app_var = interpret(app);
				let arg_var = interpret(arg);
				let mut map = Map::new();
				for (face, count) in app_var {
					let uface = face
						.to_biguint()
						.unwrap_or_else(|| panic!("Negative die count: {}", face));
					let face_map = product(&uface, &arg_var);
					for (face_in, count_in) in face_map {
						*map.entry(face_in).or_insert(Zero::zero()) += count_in * &count;
					}
				}
				map
			}
			x => panic!("Unexpected rule: {:?}. This is a bug.", x),
		}
	}

	fn convolve(a: &Map, b: &Map) -> Map {
		let mut map = Map::new();
		for n in a.keys() {
			for m in b.keys() {
				let index = n + m;
				*map.entry(index).or_insert(BigUint::from(0usize)) += &a[n] * &b[m];
			}
		}
		map
	}

	fn product(count: &BigUint, var: &Map) -> Map {
		if count == &Zero::zero() {
			Map::new()
		} else {
			let mut map = var.clone();
			let mut i = BigUint::from(1usize);
			while i < *count {
				map = convolve(&map, &var);
				i += 1usize;
			}
			map
		}
	}

	fn die(var: &Map) -> Map {
		let mut map = Map::new();
		let prod = var
			.keys()
			.product::<BigInt>()
			.to_biguint()
			.expect("Negative product. This is a bug.");
		for (sides, count) in var {
			let mult = &prod
				/ sides.to_biguint().unwrap_or_else(|| {
					panic!("Dice must have a non-negative number of sides: {}", sides)
				});
			let mut i = 1isize;
			let sides = sides
				.to_isize()
				.unwrap_or_else(|| panic!("Too many die sides: {}", sides));
			while i <= sides {
				*map.entry(BigInt::from(i)).or_insert(BigUint::from(0usize)) += count * &mult;
				i += 1;
			}
		}
		map
	}

	fn roll(var: &Map) -> Map {
		let sum = var.values().sum::<BigUint>();
		assert!(sum > Zero::zero(), "Cannot roll an empty random variable");
		let mut rng = rand::thread_rng();
		let r = rng.gen_biguint_below(&sum);
		let mut total = BigUint::zero();
		for (face, count) in var {
			total += count;
			if total > r {
				return [(face.clone(), One::one())].iter().cloned().collect();
			}
		}
		panic!("Didn't pick die face. This is a bug.")
	}

	fn bin_op(lhs: &Map, rhs: &Map, op: fn(BigInt, BigInt) -> BigInt) -> Map {
		let mut map = Map::new();
		for (lface, lcount) in lhs {
			for (rface, rcount) in rhs {
				*map.entry(op(lface.clone(), rface.clone()))
					.or_insert(Zero::zero()) += lcount * rcount;
			}
		}
		map
	}

	fn con(val: BigInt) -> Map {
		[(val, BigUint::from(1usize))].iter().cloned().collect()
	}
}

FROM rust:1.51-slim

RUN apt-get update;\
    apt-get install -y libssl-dev pkg-config;\
    cargo install wasm-pack;\
    apt-get purge -y --auto-remove libssl-dev pkg-config;\
    rm -rf /var/lib/apt/lists/* /usr/local/cargo/registry;
